package drexel.edu.pathchecker;

import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends FragmentActivity implements
        NewPathDialog.NewPathDialogListener {
    ArrayList<Map<String, String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = new ArrayList<Map<String,String>>();

        // Example input to save on typing
        HashMap<String,String> map = new HashMap<String,String>();
        map.put("From", "132 S 46th St, Philadelphia, PA");
        map.put("To", "632 N Brooklyn St,  Philadelphia, PA");

        data.add(map);


        SimpleAdapter adapter;
        adapter = new SimpleAdapter(this, data, R.layout.path_list_item,
                new String[] {"From", "To"},new int[] {android.R.id.text1,android.R.id.text2});

        ListView lv = (ListView)findViewById(R.id.listView);
        lv.setAdapter(adapter);
    }

    public void startMap(View view) {
        // Start an AsyncTask to read the GeoJSON and display potential road blocks.

        //new DetectRoadBlocksTask().execute(this);
        // TODO Copy in the MapsActivity from m.Map_test and have it extract the data from the intent
        Intent intent = new Intent(this, MapsActivity.class);
        intent.putExtra("Paths" , data);
        startActivity(intent);

    }

    /** Called when the user clicks the New Address button */
    public void newPath(View view) {
        DialogFragment newFragment = new NewPathDialog();
        newFragment.show(getSupportFragmentManager(), "newaddress");

    }

    // User clicked okay, so add the address to the list
    @Override
    public void onDialogPositiveClick(DialogFragment dialog)  {

        String from = ((EditText)dialog.getDialog().findViewById(R.id.etFrom)).getText().toString();
        String to = ((EditText)dialog.getDialog().findViewById(R.id.etTo)).getText().toString();


        HashMap<String,String>map = new HashMap<String,String>();
        map.put("From",from);
        map.put("To", to);

        data.add(map);
    }

    // User clicked the cancel button, so don't do anything
    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }


}
