package drexel.edu.pathchecker;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.

    ArrayList<Map<String, String>> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mMap = mapFragment.getMap();
        // Check if we were successful in obtaining the map.
        if (mMap != null) {
            setUpMap();
        }
        mapFragment.getMapAsync(this);
    }

    private void setUpMap() {

        mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(39.956,-75.19)));

    }

    @Override
    public void onMapReady(GoogleMap map) {

        /* TODO after combining the mapsactivity into the main project, uncomment this line to allow both activities to communicate

        */Intent intent = getIntent();
        data = (ArrayList<Map<String,String>>)intent.getSerializableExtra("Paths");

        // An example data array for testing.  Comment out after connecting to full project
/*        data = new ArrayList<Map<String, String>>();
        Map<String,String> examplePath = new HashMap<String, String>();
        examplePath.put("From", "132 S 46th St, Philadelphia, PA");
        examplePath.put("To", "632 N Brooklyn St,  Philadelphia, PA");
        data.add(examplePath);
*/
        new DownloadFilesTask().execute(this);
    }
    class DownloadFilesTask extends AsyncTask<MapsActivity, Void, JSONObject> {
        MapsActivity activity;
        List<List<LatLng>> paths;

        protected JSONObject doInBackground(MapsActivity... activities){
            activity = activities[0];

            // Get the data mapping from the calling activity
            ArrayList<Map<String,String>> data = activity.data;

            // A list of all paths, which are themselves, lists
            paths = new ArrayList<List<LatLng>>();

            // For each set of addresses, extract the To and From fields and call getPath on them
            for (Map<String,String> addresses : data) {
                String from = addresses.get("From");
                String to = addresses.get("To");
                List<LatLng> path = getPath(from, to);
                paths.add(path);
            }


            JSONObject json = null;
            String sURL="http://data.phl.opendata.arcgis.com/datasets/e10172ebe7964f63830505457c0d7c2a_0.geojson";
            URL url= null;

            HttpURLConnection request= null;
            try {
                url = new URL(sURL);
                request = (HttpURLConnection) url.openConnection();
                request.connect();
            } catch (IOException e) {
                e.printStackTrace();
            }


            try {


                BufferedReader streamReader = new BufferedReader(new InputStreamReader((InputStream) request.getContent(), "UTF-8"));
                StringBuilder responseStrBuilder = new StringBuilder();

                String inputStr;
                while ((inputStr = streamReader.readLine()) != null)
                    responseStrBuilder.append(inputStr);
                json = new JSONObject(responseStrBuilder.toString());

            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return json;
        }

        protected void onProgressUpdate() {
        }
        protected void onPostExecute(JSONObject j) {
            // When we're done parsing the JSON, iterate over its features and
            JSONArray array = null;
            try {
                array=j.getJSONArray("features");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for(int i=0;i<array.length();i++){
                String s="";
                try {
                    s=array.getJSONObject(i).getJSONObject("properties").getString("STATUS");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(s.equals("Current")) {
                    JSONArray a = null;
                    try {
                        a = array.getJSONObject(i).optJSONObject("geometry").getJSONArray("coordinates");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    LatLng position = new LatLng(39.956, -75.19);
                    try {
                        position = new LatLng(a.getJSONArray(0).getDouble(1), a.getJSONArray(0).getDouble(0));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mMap.addMarker(new MarkerOptions().position(position).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE)));
                }
            }
             //List<LatLng> path = getPath("132 S 46th St, Philadelphia, PA", "632 N Brooklyn St,  Philadelphia, PA");
            for (List<LatLng> path : paths) {
                for (LatLng coordinate : path) {
                    mMap.addMarker(new MarkerOptions().position(coordinate).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));
                }
            }
            // check for collisions on first part of the path
            mMap.addMarker(new MarkerOptions().position(findCollision(paths.get(0).get(0), paths.get(0).get(1))).title("").icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN)));

        }
    }

    // Uses Google's Directions API to get a list of coordinates that make up the path from A to B
    List<LatLng> getPath(String from, String to) {
        List<LatLng> path = new ArrayList<LatLng>();
        try {
            String sURL = "https://maps.googleapis.com/maps/api/directions/json?origin="
                    + URLEncoder.encode(from, "UTF-8") + "&destination=" + URLEncoder.encode(to, "UTF-8") + "&key=AIzaSyDRVHwbiGpWaXnGIL9Qlnb7hFviBGuffkc";
            URL url = null;

            url = new URL(sURL);
            HttpURLConnection request = (HttpURLConnection) url.openConnection();
            request.connect();

            JsonParser jp = new JsonParser();
            JsonElement root = jp.parse(new InputStreamReader((InputStream) request.getContent()));
            JsonObject rootobj = root.getAsJsonObject();
            // Get the Legs of the first route
            JsonArray legs = rootobj.get("routes").getAsJsonArray().get(0).getAsJsonObject().get("legs").getAsJsonArray();
            for (JsonElement leg : legs) {
                // For each leg, get its start_location and steps
                JsonObject location = leg.getAsJsonObject().get("start_location").getAsJsonObject();
                Double lat = location.get("lat").getAsDouble();
                Double lng = location.get("lng").getAsDouble();
                path.add(new LatLng(lat, lng));
                JsonArray steps = leg.getAsJsonObject().get("steps").getAsJsonArray();
                for (JsonElement step : steps) {
                    // For each step, log its end location and add it to the list
                    location = step.getAsJsonObject().get("end_location").getAsJsonObject();
                    lat = location.get("lat").getAsDouble();
                    lng = location.get("lng").getAsDouble();
                    path.add(new LatLng(lat, lng));
                }
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    // Returns point of collision, if no collision on paths found, return null
    LatLng findCollision (LatLng point1, LatLng point2) {
        return point1;
    }
}